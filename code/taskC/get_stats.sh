#!/bin/bash
# get statistics script to get the statistics of the logs
# cat log _ file 1 log _ file 2 ... log _ filen | ./ get _ stats. sh
#

read inp
if [ -z "$inp" ]; then
  echo "No input, run it as per above"
  exit;
else
  echo "Client Statistics"
  echo "$inp"
fi