#!/bin/bash
# Infile creation shell script to create infile directory structure.
# ./create_infiles.sh dir_name num_of_files num_of_dirs levels.
#

DIR_NAME=$1
NUM_OF_FILES=$2
NUM_OF_DIRS=$3
LEVELS=$4
TRAV=$NUM_OF_DIRS/$LEVELS

dump(){
  # call the file dump function for num files times
  for (( c=0; c<$NUM_OF_FILES; c++ )) do 
    # generate random data length
    DATA_LENGTH="$((($RANDOM % 128) + 1))"
    # generate file with length 2
    FILE_NAME="$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 4)"
    # generate random data eith length equals to DATA_LENGTH dump it to FILE_NAME
    cat /dev/urandom | tr -cd 'a-f0-9' | head -c $DATA_LENGTH > $FILE_NAME
  done
}

createDir() {
  for (( i=0; i<$NUM_OF_DIRS; i++)) do 
  # generate file with length 4
  IDIR="$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 4)"

  # create directory
  mkdir $IDIR;cd $IDIR;
  
    for (( j=0; j<$LEVELS; j++)) do
      # generate file with length 4
      LDIR="$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 4)"

      # create directory
      mkdir $LDIR;cd $LDIR;dump;cd ../

    done
  done
}

# find either directory present or not
if [ -d $DIR_NAME ]; then
  echo $DIR_NAME is already present
else 
  mkdir $DIR_NAME
  echo $DIR_NAME created
fi

cd $DIR_NAME
# call create directory
createDir