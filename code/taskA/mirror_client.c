#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>

static int opt, id, buffSize, pId;
static char *cDirectory, *iDirectory, *mDirectory, *logFile;
static int clients[256], cTotal = 0, cServed = 0;

void listFilesRecursively(char *basePath)
{
  char path[1000];
  struct dirent *dp;
  DIR *dir = opendir(basePath);

  // Unable to open directory stream
  if (!dir)
    return;

  while ((dp = readdir(dir)) != NULL) {
    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
      printf("%s\n", dp->d_name);

      // Construct new path from our base path
      strcpy(path, basePath);
      strcat(path, "/");
      strcat(path, dp->d_name);

      listFilesRecursively(path);
    }
  }
  closedir(dir);
}

void childProcessSink(int nId, int fId)
{
  char fifoName[64] = {0};
  char native[16] = {0}, foreign[16] = {0};
  strcat(fifoName, cDirectory);
  strcat(fifoName, "id");
  sprintf(native, "%d", nId);
  strcat(fifoName, native);
  strcat(fifoName, "_to_id");
  sprintf(foreign, "%d", fId);
  strcat(fifoName, foreign);
  strcat(fifoName, ".fifo");

  // create fifo to communicate with other process childs
  mkfifo(fifoName, 0666); 

  while(1){
    printf("Pid [%d] : child process is running  with %d native %d foreign\n", pId, nId, fId);
    printf("Pid [%d] : Fifo %s\n", pId, fifoName);
    listFilesRecursively(iDirectory);
    sleep(5);
  }
}

void childProcessSource(int nId, int fId)
{
  char fifoName[64] = {0};
  char native[16] = {0}, foreign[16] = {0};
  strcat(fifoName, cDirectory);
  strcat(fifoName, "id");
  sprintf(foreign, "%d", fId);
  strcat(fifoName, foreign);
  strcat(fifoName, "_to_id");
  sprintf(native, "%d", nId);
  strcat(fifoName, native);
  strcat(fifoName, ".fifo");

  // create fifo to communicate with other process childs
  mkfifo(fifoName, 0666); 
  
  while(1){
    printf("Pid [%d] : child process is running  with %d native %d foreign\n", pId, nId, fId);
    printf("Pid [%d] : Fifo %s\n", pId, fifoName);
    sleep(5);
  }
}

void getAvailableClients()
{
  // Pointer for directory entry 
  struct dirent *de;  

  // opendir() returns a pointer of DIR type.  
  DIR *dr = opendir(cDirectory);

  // opendir returns NULL if couldn't open directory
  if(dr == NULL) { 
    printf("Could not open current directory" ); 
    return;
  }

  while ((de = readdir(dr)) != NULL) {
    char *ptr = strchr(de->d_name, '.');
    if(strcmp(ptr,".id") == 0) {
      char *p = strtok(de->d_name,".");
      int unique = 1;
      for(int i = 0; i < cTotal; i++) {
        if(clients[i] == atoi(p)) {
          unique = 0;
        }
      }
      if(unique) {
        clients[cTotal++] = atoi(p);
      }
    }
  }
  closedir(dr);
}

int synchronize()
{
  FILE *fp;
  char fName[16],fPath[64], pIdStr[16];
  strcat(cDirectory, "/");
  strcpy(fPath,cDirectory);
  sprintf(fName, "%d", id);
  strcat(fName, ".id");
  strcat(fPath,fName);
  sprintf(pIdStr, "%d", pId);

  fp = fopen(fPath, "rb+");
  // if file does not exist, create it
  if(fp == NULL) {
    fp = fopen(fPath, "wb");
  } else {
    printf("process with same id is present\n");
    return -1;
  }

  // update the process id in the file
  fwrite(pIdStr, 1, sizeof(pIdStr), fp);

  // close the opened file
  fclose(fp);

  // check for the files in the common directory
  while(1){
    getAvailableClients();
    
    for(int i = cServed; i < cTotal; i++) {
      // if the client id is same no need to synchronize it
      if(clients[i] == id) {
        printf("pid [%d] : ignore same client request, go for next one\n", pId);
        cServed++;
      } else { // else synchronize it with others
        int nativeId, foreignId;
        nativeId = id;
        foreignId = clients[i];
        printf("pid [%d] : serve %d client \n", pId, foreignId);

        // create two child process to serve the client
        if(fork() == 0) {
          childProcessSource(nativeId, foreignId);
        }

        if(fork() == 0) {
          childProcessSink(nativeId, foreignId);
        }

        // make this client as served
        cServed++;
      }
    }
    sleep(1);
  }
}

int main (int argc, char **argv)
{
  int opt;
  struct stat st;

  /*get current process id*/
  pId = getpid();
  
  while ((opt = getopt (argc, argv, "n:c:i:m:b:l:")) != -1) {
    switch (opt) {
      case 'n':
        id = atoi(optarg);
        break;
      case 'c':
        cDirectory = optarg;
        break;
      case 'i':
        iDirectory = optarg;
        break;
      case 'm':
        mDirectory = optarg;
        break;
      case 'l':
        logFile = optarg;
        break;
      case 'b':
        buffSize = atoi(optarg);
        break;
    }
  }

  if(!iDirectory) {
    printf("pid [%d] : no input directory given", pId);
    return -1;
  }

  st = (const struct stat){ 0 };
  if (stat(iDirectory, &st) == -1) {
    mkdir(iDirectory, 0700);
    printf("pid [%d] : input directory created successfully\n", pId);
  } else {
    printf("pid [%d] : input directory already exists\n", pId);
    return -1;
  }

  st = (const struct stat){ 0 };
  if (stat(mDirectory, &st) == -1) {
    mkdir(mDirectory, 0700);
    printf("pid [%d] : mirror directory created successfully\n", pId);
  } else {
    printf("pid [%d] : mirror directory already exists\n", pId);
    return -1;
  }

  st = (const struct stat){ 0 };
  if (stat(cDirectory, &st) == -1) {
    mkdir(cDirectory, 0700);
    printf("pid [%d] : directory created successfully\n", pId);
  }

  return synchronize();
}
